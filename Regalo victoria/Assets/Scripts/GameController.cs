using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    public static GameController instance;
    public int row, col,countStep;
    public int rowblank,colblank;
    public int sizeRow, sizeCol;
    int countPoint=0;
    int countKey=0;
    int countComplete = 0;
    GameObject temp;
    public bool startControl = false;
    public List<GameObject> imgKeyList;
    public List<GameObject> imgPictureList;
    public List<GameObject> imgCheckPointList;
    GameObject[,] imgKeyMatriz;
    GameObject[,] imgPictureMatriz;
    GameObject[,] imgCheckPointMatriz;
    public bool checkComplete;
    bool gameIsComplete;
    public GameObject completelvl;
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        imgKeyMatriz = new GameObject[sizeRow, sizeCol];
        imgPictureMatriz = new GameObject[sizeRow, sizeCol];
        imgCheckPointMatriz = new GameObject[sizeRow, sizeCol];
        ImgOfLvl();
        ImgCheckPointManager();
        ImgKeyManager();
        for (int i = 0; i < sizeRow; i++)
        {
            for (int j = 0; j < sizeCol; j++)
            {
                if (imgPictureMatriz[i, j].name.CompareTo("blanco") == 0)
                {
                    rowblank = i;
                    colblank = j;
                    break;
                }
             
            }
        }
    }
    void ImgCheckPointManager()
    {
        for (int i = 0; i < sizeRow; i++)
        {
            for (int j = 0; j < sizeCol; j++)
            {
                imgCheckPointMatriz[i, j] = imgCheckPointList[countPoint];
                countPoint++;
            }
        }
    }
    void ImgKeyManager()
    {
        for (int i = 0; i < sizeRow; i++)
        {
            for (int j = 0; j < sizeCol; j++)
            {
                imgKeyMatriz[i, j] = imgKeyList[countKey];
                countKey++;
            }
        }
    }
    void ImgOfLvl()
    {
        imgPictureMatriz[0, 0] = imgPictureList[4];
        imgPictureMatriz[0, 1] = imgPictureList[0];
        imgPictureMatriz[0, 2] = imgPictureList[1];
        imgPictureMatriz[0, 3] = imgPictureList[2];
        imgPictureMatriz[1, 0] = imgPictureList[8];
        imgPictureMatriz[1, 1] = imgPictureList[6];
        imgPictureMatriz[1, 2] = imgPictureList[7];
        imgPictureMatriz[1, 3] = imgPictureList[11];
        imgPictureMatriz[2, 0] = imgPictureList[12];
        imgPictureMatriz[2, 1] = imgPictureList[5];
        imgPictureMatriz[2, 2] = imgPictureList[14];
        imgPictureMatriz[2, 3] = imgPictureList[10];
        imgPictureMatriz[3, 0] = imgPictureList[13];
        imgPictureMatriz[3, 1] = imgPictureList[9];
        imgPictureMatriz[3, 2] = imgPictureList[15];
        imgPictureMatriz[3, 3] = imgPictureList[3];
    }
    // Update is called once per frame
    void Update()
    {
        if (startControl)
        {
            startControl = false;

            if (countStep == 1) 
            {
                if (imgPictureMatriz[row, col] != null && imgPictureMatriz[row, col].name.CompareTo("blanco") != 0)
                {
                    if (rowblank!=row && colblank ==col)
                    {
                        if (Mathf.Abs(row - rowblank) == 1)
                        {
                            SortImg();
                            countStep = 0;
                        }
                        else
                        {
                            countStep = 0;
                        }


                    }
                    else if(rowblank==row && colblank!=col){
                        if (Mathf.Abs(col - colblank) == 1)
                        {
                            SortImg();
                            countStep = 0;

                        }
                        else
                        {
                            countStep = 0;
                        }

                    }
                    else if(rowblank==row && colblank==col || (rowblank!=row&&colblank!=col))
                    {
                        countStep = 0;
                    }                
                }
                else
                {
                    countStep = 0;
                }

            }

        }
        if (gameIsComplete)
        {
            completelvl.SetActive(true);
        }
        else
        {
            completelvl.SetActive(false);
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            gameIsComplete = true;
        }
    }
    private void FixedUpdate()
    {
        if (checkComplete)
        {
            checkComplete = false;
            for (int i = 0; i < sizeRow; i++)
            {
                for (int j = 0; j < sizeCol; j++)
                {
                    if (imgKeyMatriz[i, j].gameObject.name.CompareTo(imgPictureMatriz[i,j].gameObject.name) == 0)
                    {
                        countComplete++; 
                    }else
                    {
                        break;
                    }
                }
            }
            if (countComplete == imgCheckPointList.Count)
            {
                gameIsComplete = true;
                Debug.Log("complete");
            }
            else
                countComplete = 0;
        }
    }
    void SortImg()
    {
        temp = imgPictureMatriz[rowblank, colblank];
        imgPictureMatriz[rowblank, colblank] = null;




        imgPictureMatriz[rowblank, colblank] = imgPictureMatriz[row, col];
        imgPictureMatriz[row, col] = null;

        imgPictureMatriz[row, col] = temp;

        imgPictureMatriz[rowblank,colblank].GetComponent<ImgController>().target = imgCheckPointMatriz[rowblank, colblank];
        imgPictureMatriz[row, col].GetComponent<ImgController>().target = imgCheckPointMatriz[row, col];


        imgPictureMatriz[rowblank, colblank].GetComponent<ImgController>().startMove = true;
        imgPictureMatriz[row, col].GetComponent<ImgController>().startMove = true;

        rowblank = row;
        colblank = col;
       
    }
}
