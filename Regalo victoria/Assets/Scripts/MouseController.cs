using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseController : MonoBehaviour
{
    public int row, col;
    private void OnMouseDown()
    {
        Debug.Log("row " + row + " col " + col);
        GameController.instance.countStep += 1;
        GameController.instance.row = row;
        GameController.instance.col = col;
        GameController.instance.startControl = true;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
