using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImgController : MonoBehaviour
{
    public GameObject target;
    public bool startMove = false;
  

    // Start is called before the first frame update
    void Start()
    {
 
    }

    // Update is called once per frame
    void Update()
    {
        if (startMove)
        {

            this.transform.position = target.transform.position;
            GameController.instance.checkComplete = true;
            startMove = false;
        }
    }
}
